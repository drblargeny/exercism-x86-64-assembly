default rel

section .rodata
msg1: db "One for ", 0
msg2: db ", one for me.", 0
default_name: db "you", 0

section .text
global two_fer
; Generate a string 'One for X, one for me.' based on input
;   rdi  input name, can be null
;   rsi  buffer to write the data to, and return
;   rax  returns the buffer after the data is written
two_fer:
    ; check if input is null
    cmp rdi, 0
    jnz two_fer_name
    ; null input should use the default name
    lea rdi, [default_name]
two_fer_name:
    ; store the original buffer location to return it at the end
    push rsi ; stack:buffer
    ; store the input to use after writing the first part of the message
    push rdi ; stack:input, buffer
    ; copy part one of the message to the buffer
    lea rdi, [msg1]
    call strcpy
    ; change position in the buffer
    add rsi, rax
    ; copy the name to the buffer
    pop rdi ; stack:buffer
    call strcpy
    ; change position in the buffer
    add rsi, rax
    ; copy part two of the message to the buffer
    lea rdi, [msg2]
    call strcpy
    ; return the original buffer location
    pop rax ; stack:
    ret

; copy a 0-terminated string to a memory location
;   rdi  string, should not be null
;   rsi  memory location to write to
;   rax  return the number of characters written
strcpy:
    mov rax, -1
strcpy_loop:
    inc rax
    mov r10b, [rdi + rax]
    mov [rsi + rax], r10b
    cmp r10b, 0
    jnz strcpy_loop
    ret
section .text
; int square_of_sum(int number);
;   rdi     input number, i.e. n
;   rax     output number, i.e. f(n)^2
global square_of_sum
square_of_sum:
    ; sum of numbers is f(n) = n * (n + 1) / 2, see: https://www.wolframalpha.com/input/?i=f%28n%2B1%29%3Df%28n%29%2Bn%2B1%2Cf%280%29%3D0
    ; we need to square that, i.e. f(n)^2
    ; start with n + 1
    lea rax, [rdi + 1]  ;               f(n) = n + 1
    mul rdi         ; multiply by n     f(n) = n * (n + 1)
    sar rax, 1      ; divide by 2       f(n) = n * (n + 1) / 2
    mul rax         ; square the sum    f(n)^2 = (n * (n + 1) / 2)^2
    ret

; int sum_of_squares(int number);
;   rdi     input number, i.e. n
;   rax     output number, i.e. g(n)
global sum_of_squares
sum_of_squares:
    ; sum of squares is g(n) = n * (n + 1) * (2 * n + 1) / 6, see: https://www.wolframalpha.com/input/?i=g%28n%2B1%29%3Dg%28n%29%2B%28n%2B1%29%5E2%2Cg%280%29%3D0
    ; start with 2 * n + 1
    lea rax, [2 * rdi + 1]  ;           g(n) = 2 * n + 1
    mul rdi         ; multiply by n     g(n) = n * (2 * n + 1)
    inc rdi         ; change rdi from n to n + 1
    mul rdi         ; multiply by n + 1 g(n) = n * (n + 1) * (2 * n + 1)
    mov rdi, 6      ; change rdi to 6
    div rdi         ; divide by 6       g(n) = n * (n + 1) * (2 * n + 1) / 6
    ret

; int difference_of_squares(int number);
;   rdi     input number, i.e. n
;   rax     output number, i.e. h(n)
global difference_of_squares
difference_of_squares:
    ; based on definitions above, difference of squares is h(n) = f(n)^2 - g(n)
    push    rdi             ; preserve n for the second call
    call    sum_of_squares  ; call g(n), result in rax
    pop     rdi             ; restore n in rdi
    push    rax             ; store g(n) result
    call    square_of_sum   ; call f(n)^2
    pop     rdi             ; restore g(n) in rdi
    sub     rax, rdi        ; subtract f(n)^2 - g(n)
    ret

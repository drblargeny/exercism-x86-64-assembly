section .text
; void reverse(char *str);
;   rdi     pointer to string to reverse, will be used as pointer to characters at the front of the string
global reverse
reverse:
    ; check for null input
    cmp rdi, 0
    jz .return ; found null, don't do anything
;   rsi     pointer to characters at the end of the string, first need to find the end of the string
    mov rsi, rdi - 1 ; begin at one before the start of the string to eliminate a jump operation
.find_end_of_string: ; loop to search for the last character address before the terminating 0
    inc rsi ; move to next character in string, first iteration will put this at the start
    ; check for the terminating 0 character
    cmp [rsi], byte 0
    jnz .find_end_of_string ; not found, continue loop
.found_end_of_string: ; found terminating 0 character
    dec rsi ; set the end pointer to the byte before the terminating 0
.swap: ; loop to swap the character at the start pointer with the character at the end pointer
    ; check to see if the start pointer >= the end pointer, which is the halfway point in the string
    cmp rdi, rsi
    jge .return ; reached the halfway point, no more characters to swap
    ; temporarily store start/end characters in registers for the swap
    mov r8b, [rdi]
    mov r9b, [rsi]
    ; swap the start/end characters in the string
    mov [rdi], r9b
    mov [rsi], r8b
    inc rdi ; move the start to the next character
    dec rsi ; move the end to the previous character
    jmp .swap ; continue loop
.return: ; operations complete
    ret

section .text
; void rotate(const char *text, int shift_key, char *buffer);
;   rdi     input string text
;   rsi     shift key
;   rdx     output string buffer
global rotate
rotate:
    test rdi, rdi   ; test for null input string
    jz  .return     ; found null input, exit
    test rdx, rdx   ; test for null output buffer
    jz  .return     ; found null buffer, exit
    dec rdi         ; decrease the input pointer so it can be increased at the beginning of the loop
;   r8      r8 will be used to hold the pointer to the output buffer because rdx is used by the cqo and idiv operations
    lea r8, [rdx - 1]   ; move rdx to r8 and decrease it so it can be increased at the beginning of the loop
;   r9      r9 will store a constant of 26 for use with idiv and cmov_ when handling overflow/underflow outside the a-z characters
    mov r9, 26      ; store constant of 26
;   rdx:rax will be used as input for the idiv operation
    mov rax, rsi    ; move shift amount into rax for idiv operation
    cqo             ; extend the sign of rax into rdx for signed division
    idiv r9         ; divide the shift amount by 26
;   rax     now holds the quotient of the division
;   rdx     now holds the remainder of the division (i.e., modulus)
    lea rsi, [rdx + r9]     ; add 26 to the remainder as the new shift amount, guarantees a positive rotation, even if it rotates more than once
.loop:              ; loop to read all the characters from the input
    inc rdi         ; move to the next character of the input
    inc r8          ; move to the next character of the output
    movzx rcx, byte [rdi]   ; read the current character from the input
    test rcx, rcx   ; check to see if the current character terminates the string
    jz  .end_of_string      ; found end of the string, break out of the loop
;   r10     will hold the letter index of the character read from the input (e.g. 'a' has index of 0 and 'z' has index 25)
    mov r10, rcx    ; store the current character into r10
    or  r10, 0x20   ; convert alphabetic ASCII characters to lowercase
    sub r10, 'a'    ; map the lowercase character to its index with respect to 'a'
    jl  .copy       ; values lower than 'a' aren't alphabetic and don't need to be rotated
    cmp r10, 'z' - 'a'      ; check the letter index value against the index for 'z'
    jg  .copy       ; values greater than 'z' aren't alphabetic and don't need to be rotated
.rotate:            ; rotates the orignal character in rcx based on the lowercase version in rax
;   rax     will be used to rotate the letter index using division (i.e., modulus)
    lea rax, [r10 + rsi]    ; shift the letter index and store it in rax to rotate
    cqo             ; expand the letter index to prepare for idiv
    idiv r9         ; divide by 26, the remainder (i.e. modulus) will be the properly rotated letter index
    sub rdx, r10    ; calculate the difference from the rotated letter index to the original letter index.  that will be the correct shift amount
    add rcx, rdx    ; add the shift amount to the orignal character
.copy:              ; copies the character into the output buffer
    mov [r8], cl    ; move the character in rcx/cl to the current position in the output buffer
    jmp .loop       ; continue the loop over the input string
.end_of_string:     ; ends the loop after string termination is detected
    mov [r8], cl    ; move the current character (i.e., string terminator) to the current position in the output buffer
.return:            ; exits the function
    ret

section .text
; int is_isogram(const char *str);
;   rdi     input string
;   rax     return true(-1) if string is an ASCII isogram, otherwise false(0)
global is_isogram
is_isogram:
    xor rax, rax    ; default the result to false(0)
    cmp rdi, 0      ; check if input is null
    jz  .return     ; stop processing when input is null
;   rcx     bit field to track usage of letters
    mov rcx, -1     ; set all bits to one
    dec rdi         ; move pointer to previous character for loop
.loop:
    inc rdi         ; move pointer to next character
;   rsi     stores the current character from the string
    movzx rsi, byte [rdi]   ; read the next character
    cmp rsi, 0      ; check if the string terminated
    jz  .end_of_string
    or  rsi, 0x20   ; convert character to lower case
    sub rsi, 'a'    ; map 'a' to 0
    js  .loop       ; character < 'a', don't count it as alphabetic
    cmp rsi, 'z' - 'a'      ; check if the character is > 'z'
    jg  .loop       ; character > 'z', don't count it as alphabetic
    btr rcx, rsi    ; reset the bit field for the character
    jnc .return     ; if bit was already reset, stop processing
    jmp .loop       ; continue loop to check the next character
.end_of_string:     ; string terminated without a repeat character
    mov rax, -1     ; set return value to true(-1)
.return:            ; return the result
    ret

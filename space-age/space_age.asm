default rel
section .rodata

; planet orbits in Earth years
planets:
.MERCURY dd 0.2408467
.VENUS dd 0.61519726
.EARTH dd 1.0
.MARS dd 1.8808158
.JUPITER dd 11.862615
.SATURN dd 29.447498
.URANUS dd 84.016846
.NEPTUNE dd 164.79132

; number of seconds in an Earth year
earth_year_seconds: dq 31557600

section .text
; float age(enum planet planet, int seconds);
;   rdi     planet index for planets array
;   rsi     integer seconds
;   xmm0    return float number of years on specified planet
global age
age:
    ; earth_years = seconds / earth_year_seconds
    cvtsi2ss xmm0, rsi
    cvtsi2ss xmm1, [earth_year_seconds]
    divss xmm0, xmm1
    ; planet_years = earth_years / planets[planet]
    mov rcx, planets
    movss xmm1, [rcx + rdi * 4]
    divss xmm0, xmm1
    ret

section .data

; create a mapping table to convert the DNA characters to RNA characters
; NOTE: having these mappings here increases the size of the executable
dna_to_rna_map:
times 'A'-0 db 0
db 'U' ; map A -> U
times 'C'-'B' db 0
db 'G' ; map C -> G
times 'G'-'D' db 0
db 'C' ; map G -> C
times 'T'-'H' db 0
db 'A' ; map T -> A
times 256-'U' db 0

section .text

; void to_rna(const char *strand, char *buffer);
;   rdi     input RNA string
;   rsi     output string buffer
global to_rna
to_rna:
    ; check if output buffer is null
    cmp rsi, 0
    jz .return ; do nothing since there's no buffer to write to
;   r9      store initial buffer address in case we detect bad input and need to write an empty string
    mov r9, rsi
    ; check if input is null
    cmp rdi, 0
    jz .empty_string ; input is null, return empty string as output
;   rax     store the address of the DNA->RNA mapping table
    mov rax, dna_to_rna_map
;   r8      qword version of r8b, used in address arithmetic for lookup table
    xor r8, r8 ; set all bits to 0, only the lowest byte will be updated when characters are read
.loop:
;   r8b     store the current character from input string
    mov r8b, [rdi]
    ; check for end of input string
    cmp r8b, 0
    jz .end_string ; input string ended, terminate the output string
    ; map the input charater to an output character
    mov r8b, [rax + r8]
    ; check that it mapped to something valid
    cmp r8b, 0
    jz .empty_string ; invalid mapping, return an empty string
    ; write the mapped character
    mov [rsi], r8b
    ; increment input/output locations
    inc rdi
    inc rsi
    ; continue loop
    jmp .loop
.empty_string: ; return an empty output string
    mov rsi, r9 ; restore the initial buffer addres in rsi
.end_string: ; terminate the output string
    mov [rsi], byte 0 ; write terminating byte to buffer
.return:
    ret

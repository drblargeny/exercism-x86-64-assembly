; define the board size
%define BOARD_SIZE    64

section .text
; uint64_t square(int64_t number);
;   rdi     square number
;   rax     return total grains on square
global square
square:
    xor rax, rax        ; start at 0
    dec rdi             ; subtract 1 from the input number
    js .out_of_bounds   ; when < 0, it's out of bounds
    cmp rdi, BOARD_SIZE ; compare with the board size
    jge .out_of_bounds  ; when > board size, it's out of bounds
    ; values on square correspond to powers of two, so each square can be represented as a bit
    bts rax, rdi        ; set the bit for the specified square
.out_of_bounds:
    ret

; uint64_t total(void);
;   rax     return total grains on board
global total
total:
    mov rax, -1         ; set all bits since every bit corresponds a square
    ; the result should be the total for the board
    ret


default rel

section .rodata

item:
.MAX_ITEMS equ 8

section .bss

struc item_list
    .size: resd 1
    .items: resd item.MAX_ITEMS;
endstruc


section .text

; int allergic_to(enum item item, unsigned int score);
;   rdi     item
;   rsi     score
;   rax     0 if score doesn't contain the item, -1 if it does
global allergic_to
allergic_to:
    xor rax, rax ; init return value to 0/false (generally faster than mov)
;   r8      true return value, used by cmovc operation
    mov r8, -1
    cmp rdi, 0
    jl .return ; item < 0, return false
    cmp rdi, item.MAX_ITEMS
    jge .return ; item > MAX_ITEMS, return false
    bt rsi, rdi ; test the score bit corresponding to the item value
    cmovc rax, r8 ; if bit is set, return true
.return:
    ret

; void list(unsigned int score, struct item_list *list);
;   rdi     score
;   rsi     pointer to an item_list to populate
global list
list:
;   rcx     index/counter for item_list.items and item_list.size
    xor rcx, rcx ; init to 0 (generally faster than mov) 
.loop:
    cmp rdi, 0
    jz .return ; score is 0, return
    bsf rax, rdi ; check for lowest set bit
    cmp rax, item.MAX_ITEMS
    jge .return ; lowest set bit is above the one's we're checking, return
    ; store the lowest set bit into the item_list.list (dword/int size)
    mov [rsi + item_list.items + rcx * 4], eax
    inc rcx ; increase index/counter
    blsr rdi, rdi ; reset the lowest set bit
    jmp .loop ; resume loop
.return:
    ; store the index/counter into the item_list.size (dword/int size)
    mov [rsi + item_list.size], ecx
    ret

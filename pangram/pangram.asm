section .data
expected_bits equ (0x01 << 26) - 1

section .text
; int is_pangram(const char *str);
;   rdi     input string
;   rax     false (0) if not a pangram, otherwise true (-1)
global is_pangram
is_pangram:
;   rcx     bit field to track all the letters
    xor rcx, rcx ; set all bits to 0
    ; check for null input
    cmp rdi, 0
    jz .end_loop ; found null, return false
    dec rdi ; decrease pointer to elimnate a jump operation in the loop
.loop:
    inc rdi ; move to the next character
;   rsi     stores the current character
    movzx rsi, byte [rdi] ; read the next character, clearing the higher bytes to use the sign change later
    ; check if the string was terminated with 0
    cmp rsi, 0
    jz .end_loop
    ; convert to lower case
    or rsi, 0x20 ; A and a differ by 0x20, and A,Z doesn't have the 0x20 bit set; so can use 'or' to convert to lowercase
    sub rsi, 'a' ; shift a to 0 for bit field value
    js .loop ; value went negative (i.e. character < a), continue to the next character
.set_bit_field: ; character is in range to set a bit field, this includes characters > z
    bts rcx, rsi ; set a bit field for the character where both A and a map to 0
    jmp .loop ; continue loop
.end_loop: ; no more characters to check in the string
    and rcx, expected_bits ; only keep the bits we're interested in (i.e. a-z); this ignores bits set for characters > z
    cmp rcx, expected_bits ; compare the bit field with what we expect if all letters were found
;   al      byte mode version of the rax register, used because setX commends operate on bytes
    setne al ; set return to 0 if all bits matched and 1 if they didn't
    dec al ; decrease return value so it's -1 if all bits matched and 0 if they didn't
    movsx rax, al ; extend into the entire register
    ret

section .text
; int find(int *array, int size, int value);
;   rdi     pointer to the array
;   rsi     size of the array
;   rdx     value to search for
;   rax     return -1 when value isn't found, otherwise the index where the value was found
global find
find:
    test rdi, rdi           ; check for a null array, test x,x is slightly faster than cmp x,0
    jz  .value_not_found    ; stop if the array is null
;   rcx     store the original array start so we can calculate the correct index at the end
    mov rcx, rdi
.loop:
    test rsi, rsi           ; check length of array
    jle .value_not_found    ; zero/negative length means no match
    mov rax, rsi            ; copy the array size
    shr rax, 1              ; divide the current array size in half to get the middle index to check
;   edx     dword/int version of rdx    
    cmp edx, dword [rdi + rax * 4]    ; test the value at the current index
    jl  .lesser             ; when target values is less than value at current index
    jg  .greater            ; when target values is greater than value at current index
.equal:                     ; value matched, return the index of the current element based on the original array start
    sub rdi, rcx            ; get diference of current array start and the original array start
    shr rdi, 2              ; divide it by 4: the difference is in bytes, but we need it in dwords
    add rax, rdi            ; add the difference to the current index used 
    ret
.lesser:                    ; continue search at lower indices in the array
    mov rsi, rax            ; make the current index, the new size (upper bound) of the array
    jmp .loop               ; continue loop with new bounds for the array
.greater:                   ; continue search at higher indices in the array
    inc rax                 ; get the index of the next element
    sub rsi, rax            ; decrease the array size by the number of elements before the next element
    lea rdi, [rdi + rax * 4]    ; shift the array start to the next element
    jmp .loop               ; continue loop with new bounds for the array
.value_not_found:
    mov rax, -1 ; set return value to indicate value wasn't found
    ret

section .text
; int distance(const char *strand1, const char *strand2);
;   rdi     pointer to string a
;   rsi     pointer to string b
;   rax     count of different characters, -1 if string lengths are different
global distance
distance:
    mov rax, -1 ; start at -1, will set to 0 at beginning of loop
;   rcx     index into both strings
    mov rcx, -1 ; start at -1, will set to 0 at beginning of loop
.loop_different:
    inc rax ; increase count of differences; sets to 0 at start
.loop:
    inc rcx ; increase index into strings; sets to 0 at start
    ; load characters from strings into registers
    mov r8b, [rdi + rcx]
    mov r9b, [rsi + rcx]
    ; check if characters match
    cmp r8b, r9b
    jne .different 
.matched: ; characters match
    ; check if strings terminated
    cmp r8b, 0
    jnz .loop ; not at end of strings, continue loop
    ret ; end of both strings, return the difference count
.different: ; characters differ
    ; check if either string terminated
    cmp r8b, 0
    jz .error ; first string ended before the second
    cmp r9b, 0
    jnz .loop_different ; not at end of strings, increment difference and continue loop
.error: ; string lengths differ
    ; return -1
    mov rax, -1
    ret

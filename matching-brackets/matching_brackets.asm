default rel
section .data

section .text
; int is_paired(const char *str);
;   rdi     pointer to the input string
;   rax     return true (-1) if brackets are matched in the string, false (0) otherwise
global is_paired
is_paired:
;   rax     before using this for the return value, it will store the stack pointer so we can check the size of the stack later and clear it with a single mov
    mov rax, rsp    ; store the original stack pointer
    test rdi, rdi   ; check if it's a null string
    jz  .return     ; null string found, exit
;   rdi     as we progress through the string, we'll just change the value of rdi instead of using an index into it
    dec rdi         ; move the string pointer back so it can be incremented at the start of the loop
.loop:              ; loop to read and check the characters in the string
    inc rdi         ; move to the next character
;   sil     stores the current character from the string
    mov sil, [rdi]  ; read the character into a register to avoid repeatedly reading memory
    test sil, sil   ; check if the current character is 0, which terminates the string
    jz  .return     ; terminated string found, exit
;   cx      used to identify the type of brackets found, 0=(), 1=[], 2={}
    xor cx, cx      ; start with 0=()
    cmp sil, '('    ; check for opening (
    je  .open       ; found an opening character
    cmp sil, ')'    ; check for closing )
    je  .close      ; found a closing character
    inc cx          ; change to 1=[]
    cmp sil, '['    ; check for opening [
    je  .open       ; found an opening character
    cmp sil, ']'    ; check for closing ]
    je  .close      ; found a closing character
    inc cx          ; change to 2={}
    cmp sil, '{'    ; check for opening {
    je  .open       ; found an opening character
    cmp sil, '}'    ; check for closing }
    jne .loop       ; no brackets found, continue loop
.close:             ; found a closing character
    cmp rax, rsi    ; check to see if there are values on the stack
    je  .failed     ; empty stack detected, this means we found a closing bracket before any opening brackets
    pop dx          ; pull the last seen bracket type from the stack
    cmp cx, dx      ; check if the current bracket type matches the previous one
    je  .loop       ; current an previous brackets matched, i.e. successful open/close pair; continue loop
.failed:            ; failed to find balanced/paired brackets
    inc rsp         ; change the stack pointer so it's different from the original value, this indicates that the stack was not reset by finding matcehd pairs of brackets
.return:            ; prepare the result and exit
    cmp rsp, rax    ; check to see if the stack has changed
;   sil     this will now be used to store a 1 or 0 based on whether the stack changed
    setne sil       ; set sil to 1 if the stack changed (i.e. failure/mismatch), otherwise 0
    dec sil         ; decrease sil so the failure/mismatch becomes 0 and success/matched becomes -1, this aligns with our expected return values
    mov rsp, rax    ; retore the original stack pointer before leaving
    movsx rax, sil  ; sign extend the value in sil and use it as the return value
    ret
.open:              ; found an opening character
    push cx         ; push its bracket type onto the stack
    jmp .loop       ; continue the loop

section .rodata

; define strings for the colors
black: db 'black', 0
brown: db 'brown', 0
red: db 'red', 0
orange: db 'orange', 0
yellow: db 'yellow', 0
green: db 'green', 0
blue: db 'blue', 0
violet: db 'violet', 0
grey: db 'grey', 0
white: db 'white', 0

; define an array of colors, use 0 to terminate the array (like strings)
colors_array: dq black, brown, red, orange, yellow, green, blue, violet, grey, white, 0

section .text

; int color_code(const char *color);
;   rdi pointer to color string
;   rax return color number
global color_code
color_code:
;   rcx counter to index colors_array, start at -1 so inc can set to 0
    mov rcx, -1
.loop:
    inc rcx
;   rsi pointer to current string in colors_array
    ; couldn't load in one [colors_array + rcx * 8] operation; so split into two
    mov rsi, colors_array
    mov rsi, [rsi + rcx * 8]
    cmp rsi, 0
    jz .invalid ; reached end of colors list, input didn't match
    push rcx ; preserve counter and align stack before call
    call equals
    pop rcx ; restore counter
    cmp rax, 0
    jz .loop ; equals didn't find a match, check next color in colors_array
.valid:
    mov rax, rcx ; return the counter/index of the match that was found
    ret
.invalid:
    mov rax, -1 ; no matches found, return -1
    ret

; int compare(char *a, char *b)
;   rdi pointer to string a
;   rsi pointer to string b
equals:
;   rcx counter to index both strings
    mov rcx, -1
.loop:
    inc rcx
;   r8b current byte from string a
    mov r8b, [rdi + rcx]
;   r9b current byte from string b
    mov r9b, [rsi + rcx]
    cmp r8b, r9b
    jne .false ; a[rcx] != b[rcx] ; strings differ
    cmp r8b, 0
    jnz .loop ; a[rcx] == b[rcx] != 0 ; within identical strings
.true: ; a[rcx] == b[rcx] == 0 ; end of identical strings
    mov rax, -1
    ret
.false:
    mov rax, 0
    ret

; const char **colors(void);
;   rax return pointer to colors_array
global colors
colors:
    mov rax, colors_array
    ret

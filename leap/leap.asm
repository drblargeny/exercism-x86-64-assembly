section .text
global leap_year
; int leap_year(int year)
;   rdi     input year
;   rax     output 0 if not a leap year and -1 if it is
leap_year:
    ; on every year that is evenly divisible by 4
    ; true if lowest two bits are both 0
    mov rax, 0x03
    and rax, rdi
    cmp rax, 0
    jnz leap_year_false
    ; except every year that is evenly divisible by 100
    mov rdx, 0
    mov rax, rdi
    mov rsi, 100
    idiv rsi
    cmp rdx, 0
    jnz leap_year_true
    ; unless the year is also evenly divisible by 400
    ; already divided by 100, check if the result is divisible by 4
    mov rsi, 0x03
    and rax, rsi
    cmp rax, 0
    jnz leap_year_false
leap_year_true:
    mov rax, -1
    ret
leap_year_false:
    mov rax, 0
    ret
